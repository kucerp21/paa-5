import random
import math

INIT_TEMP = 1000
COOLING_FACTOR = 0.95
MIN_TEMP = 1
EQUILIBRIUM = 3
COST_RATION = 0.5


class State:
    def __init__(self, state, weight, cost, satisfied_ratio):
        self.configuration = state
        self.weight = weight
        self.cost = cost
        self.satisfied_ratio = satisfied_ratio


def clause_satisfied(clause, state):
    satisfied = False
    for literal in clause:
        if literal < 0 and state[abs(literal) - 1] == 0:
            satisfied = True
        elif literal > 0 and state[abs(literal) - 1] == 1:
            satisfied = True
    return satisfied


class SAT3:
    def __init__(self, input_file, it=INIT_TEMP, cf=COOLING_FACTOR, eq=EQUILIBRIUM, mt=MIN_TEMP, cr=COST_RATION):
        self.temperature = it
        self.cooling_factor = cf
        self.min_temp = mt
        self.cost_ratio = cr
        self.state_count = 0
        self.weights = []
        self.clauses = []
        self.variable_count = 0
        self.clause_count = 0
        self.best_state = None
        self.best_cost = 0
        self.state = None
        self.load_instance(input_file)
        self.total_weight = sum(self.weights)
        self.equilibrium = eq * self.variable_count  # eq*N

    def solve(self):
        self.state = self.get_init_state()

        while self.temperature > self.min_temp:  # not frozen
            for i in range(self.equilibrium):  # equilibrium
                neighbour_state = self.get_neighbour_state()
                self.state_count += 1

                if neighbour_state.cost > self.state.cost:
                    self.state = neighbour_state
                elif math.e ** ((neighbour_state.cost - neighbour_state.cost) / self.temperature) >= random.random():
                    self.state = neighbour_state

                if self.best_state is None:
                    self.best_state = self.state
                elif self.best_state.cost < self.state.cost and self.best_state.satisfied_ratio <= self.state.satisfied_ratio:
                    self.best_state = self.state

            self.cool()  # cool

        return self.best_state

    def satisfied(self, state):
        for clause in self.clauses:
            if not clause_satisfied(clause, state):
                return False
        return True

    def get_neighbour_state(self):
        position = random.randint(0, self.variable_count-1)
        neighbour_configuration = self.state.configuration
        neighbour_configuration[position] = (neighbour_configuration[position] + 1) % 2
        cost, satisfied_ratio = self.get_cost_and_satisfied_ratio(neighbour_configuration)
        return State(neighbour_configuration, self.get_weight(neighbour_configuration), cost, satisfied_ratio)

    def get_weight(self, configuration):
        total_weight = 0
        for i, val in enumerate(configuration):
            if val == 1:
                total_weight += self.weights[i]
        return total_weight

    # cost function that also evaluates quality of configurations, that do not satisfy all the clauses
    def get_cost_and_satisfied_ratio(self, configuration):
        clauses_satisfied = 0
        for clause in self.clauses:
            if clause_satisfied(clause, configuration):
                clauses_satisfied += 1
        satisfied_clause_ration = float(clauses_satisfied) / self.clause_count
        total_weight_ratio = float(self.get_weight(configuration)) / self.total_weight

        return self.cost_ratio * satisfied_clause_ration + (1 - self.cost_ratio) * total_weight_ratio, satisfied_clause_ration

    def cool(self):
        self.temperature = self.temperature * self.cooling_factor

    def get_init_state(self):
        configuration = []
        for x in range(0, self.variable_count):
            configuration.append(random.randint(0, 1))
        # state = [0] * self.variable_count
        cost, satisfied_ratio = self.get_cost_and_satisfied_ratio(configuration)
        return State(configuration, self.get_weight(configuration), cost, satisfied_ratio)

    def load_instance(self, input_file):
        if_stream = open(input_file, 'r')

        while True:
            line = if_stream.readline()
            if not line:
                break
            parsed_line = line.strip().split(" ")

            if parsed_line[0].strip() == 'c':
                continue
            elif parsed_line[0].strip() == 'p':
                self.variable_count = int(parsed_line[2])
                self.clause_count = int(parsed_line[3])
            elif parsed_line[0].strip() == 'w':
                self.weights = [int(w) for i, w in enumerate(parsed_line) if w != 'w']
                self.weights = self.weights[:-1]
            elif parsed_line[0].strip() == '%':
                break
            else:
                clause = [int(x) for x in parsed_line]
                clause = clause[:-1]
                self.clauses.append(clause)

    def print_instance(self):
        print('v:', self.variable_count)
        print('c:', self.clause_count)
        print('w:', *self.weights)
        print('clauses:')
        for clause in self.clauses:
            print(*clause)

    def print_solution(self):
        print('weight: ', self.min_total_weight)
        print('config: ', *self.configuration)
