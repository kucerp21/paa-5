#!/usr/bin/env python3.7

import sys
import getopt
import time

from sat3 import SAT3

USAGE_MSG = 'test.py -i <input_file> (-s <solutions_file> | -S) -t <initial_temp> -c <cooling_factor> -m <min_temp> -e <equilibrium> -r <cost_ratio>'


def main(argv):
    input_file = ''
    solutions_file = ''
    init_temp = None
    cooling_factor = None
    min_temp = None
    equilibrium = None
    cost_ratio = None
    check_solution = True
    try:
        opts, args = getopt.getopt(argv, "Sdhi:s:t:c:m:e:r:", ["ifile="])
    except getopt.GetoptError:
        print(USAGE_MSG)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(USAGE_MSG)
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_file = arg
        elif opt in ("-s", "--sfile"):
            solutions_file = arg
        elif opt in ("-d", "--headers"):
            print_headers()
            sys.exit()
        elif opt == "-t":
            init_temp = int(arg)
        elif opt == "-c":
            cooling_factor = float(arg)
        elif opt == "-m":
            min_temp = float(arg)
        elif opt == "-e":
            equilibrium = int(arg)
        elif opt == "-r":
            cost_ratio = float(arg)
        elif opt == "-S":
            check_solution = False
        else:
            print(USAGE_MSG)
            sys.exit(2)

    if input_file == '' or init_temp is None or cooling_factor is None or min_temp is None or equilibrium is None or cost_ratio is None:
        print(USAGE_MSG)
        sys.exit(2)

    solve_sat3(input_file, solutions_file, init_temp, cooling_factor, min_temp, equilibrium, cost_ratio, check_solution)


def solve_sat3(input_file, solutions_file, init_temp, cooling_factor, min_temp, equilibrium, cost_ratio, check_solution):
    problem_name = input_file.split('/')[2].split('.')[0]
    sat_instance = SAT3(input_file, init_temp, cooling_factor, equilibrium, min_temp, cost_ratio)

    start = time.process_time()
    sat_instance.solve()
    end = time.process_time()
    total_time = end - start

    if check_solution:
        relative_fault, weight_ratio = get_relative_fault_and_weight_ratio(sat_instance, solutions_file, problem_name)
    else:
        relative_fault, weight_ratio = 0, 0
    print(problem_name + ',' + str(total_time) + ',' + str(sat_instance.state_count) + ',' + str(
        relative_fault) + ',' + str(sat_instance.best_state.satisfied_ratio) + ',' + str(weight_ratio))


def get_relative_fault_and_weight_ratio(solved_sat, solutions_file, problem_name):
    if_stream = open(solutions_file, 'r')
    while True:
        line = if_stream.readline()
        if not line:
            break

        parsed_line = line.strip().split(" ")
        if parsed_line[0] == problem_name[1:]:
            return relative_fault(solved_sat.best_state.weight, parsed_line[1]), solved_sat.best_state.weight/int(parsed_line[1])


def relative_fault(approximated, optimal):
    return abs(int(approximated) - int(optimal)) / max(int(approximated), int(optimal))


def print_headers():
    print("in_file,process_time,state_count,relative_fault,satisfied_clauses_ratio,optimum_weight_ratio")


if __name__ == "__main__":
    main(sys.argv[1:])
