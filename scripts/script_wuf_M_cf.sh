#!/bin/sh
set -x

OUT_FILE=results/result_wuf20_78_m_cf

./run_script.py -d > $OUT_FILE

echo 'cf 0.85' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 1000 -c 0.85 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'cf 0.90' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 1000 -c 0.90 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'cf 0.95' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 1000 -c 0.95 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'cf 0.98' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 1000 -c 0.98 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'cf 0.99' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 1000 -c 0.99 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done