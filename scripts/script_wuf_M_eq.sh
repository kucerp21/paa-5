#!/bin/sh
set -x

OUT_FILE=results/result_wuf20_78_m_eq

./run_script.py -d > $OUT_FILE

echo 'eq 1' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 1000 -c 0.95 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'eq 2' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 1000 -c 0.95 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'eq 3' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 1000 -c 0.95 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'eq 5' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 1000 -c 0.95 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'eq 10' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 1000 -c 0.95 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done