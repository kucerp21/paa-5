#!/bin/sh
set -x

OUT_FILE=results/result_wuf20_78_n_n

./run_script.py -d > $OUT_FILE

echo '20' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-N/wuf20-0$(echo $num).mwcnf -S -t 500 -c 0.9 -m 1 -e 2 -r 0.75 >> $OUT_FILE
done

echo '50' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf50-201-N/wuf50-0$(echo $num).mwcnf -S -t 500 -c 0.9 -m 1 -e 2 -r 0.75 >> $OUT_FILE
done

echo '75' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf75-310-N/wuf75-0$(echo $num).mwcnf -S -t 500 -c 0.9 -m 1 -e 2 -r 0.75 >> $OUT_FILE
done