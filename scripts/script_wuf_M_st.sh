#!/bin/sh
set -x

OUT_FILE=results/result_wuf20_78_m_st

./run_script.py -d > $OUT_FILE

echo 'st 100' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 100 -c 0.95 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'st 200' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 200 -c 0.95 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'st 500' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 500 -c 0.95 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'st 2000' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 2000 -c 0.95 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done

echo 'st 10000' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 10000 -c 0.95 -m 1 -e 3 -r 0.5 >> $OUT_FILE
done