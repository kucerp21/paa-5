#!/bin/sh
set -x

OUT_FILE=results/result_wuf20_78_m_n

./run_script.py -d > $OUT_FILE

echo '20' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf20-78-M/wuf20-0$(echo $num).mwcnf -s test_data/wuf20-78-M-opt.dat -t 500 -c 0.9 -m 1 -e 2 -r 0.75 >> $OUT_FILE
done

echo '50' >> $OUT_FILE
for num in 123 12 133 139 241 24 274 369 36 405 414
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf50-201-M/wuf50-0$(echo $num).mwcnf -s test_data/wuf50-201-M-opt.dat -t 500 -c 0.9 -m 1 -e 2 -r 0.75 >> $OUT_FILE
done

echo '75' >> $OUT_FILE
for num in $(seq 1 20)
do
  # shellcheck disable=SC2046
  # shellcheck disable=SC2116
  ./run_script.py -i test_data/wuf75-310-M/wuf75-0$(echo $num).mwcnf -S -t 500 -c 0.9 -m 1 -e 2 -r 0.75 >> $OUT_FILE
done